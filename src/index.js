const configPROD = require('../configs/prod.json');
const configDEV = require('../configs/dev.json');
const readline = require('readline');

const rollbackFn = require('./rollback');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const QUESTION = 'Deseja rodar o script em prod ou dev? Digite "PROD CUIDADO FDP" ou "DEV" para responder.. \n';

init();

/**
 * Main app function
*/
function init() {
    rl.question(QUESTION, (answer) => {
        rl.close();
        action(answer);
    });
}

function action(answer) {
    if (answer == 'PROD CUIDADO FDP') {
        rollbackFn(configPROD);
    } else if (answer == 'DEV') {
        rollbackFn(configDEV);
    } else {
        console.log('Opção inválida');
        return;
    }
}