const iconv = require('iconv-lite');
const rimraf = require('rimraf');
const fs = require('fs');
const axios = require('axios');
const { execSync } = require('child_process');

const uuids_rollback = require('../ids_rollback.json');

let config;

/** 
 * Main app function
 * 
*/
function rollback(configToBeUsed) {
    config = configToBeUsed;
    for (let i in uuids_rollback) {
        const uuid = uuids_rollback[i];
        console.log(uuid);
        rollbackRegionAreas(uuid);
        rollbackTimeOnMerchant(uuid);        
    }
}

function rollbackRegionAreas(uuid) {
    console.log(`Deleting stages and boundaries parameters to UUID : "${uuid}"`);
    execSync(`psql "service=${config.REGION_SERVICE_NAME}" -c "delete from deliveries where mode = 'STAGE' and owner_id = '${uuid}'"`);
    execSync(`psql "service=${config.REGION_SERVICE_NAME}" -c "delete from boundaries_parameters where owner_id = '${uuid}'"`);
    execSync(`psql "service=${config.REGION_SERVICE_NAME}" -c "update deliveries set time_formula = 0 where status = 'ACTIVE' and owner_id = '${uuid}'"`);
}

function rollbackTimeOnMerchant(uuid) {
    let url = `${config.URL_MERCHANT_SERVICE}/stores/${uuid}/delivery-time:rollback?system-id=ScriptRollback&user-id=ricardo.faria`;
    axios.post(url).then(result => {
        console.log(`Delivery time rolled back for restaurant "${uuid}"`)
    }).catch(error => {
        throw error;
    });
}

module.exports = rollback;